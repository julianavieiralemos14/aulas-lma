const {createApp} = Vue;

createApp({
    data(){
        return{
            username: "",
            password: "",
            error: null,
            sucesso:null,

            //arrays para armazenamento dos usuários e senhas
            
            usuarios: ["admin", "mumuzinho"],
            senhas:["1234", "1234"],

            //Variáveis para armazenamento do usuário e senha que será cadastrado 
            newUsername: "",
            newPassword: "",
            confirmPassword: "",

            //Interação das mensagens de texto
            mostrarEntrada: false,

            mostrarLista: false,

        } // Fechamento return
    }, //Fechamento data 

    methods:{
        login(){
            setTimeout(() => {
                if((this.username === 'Mumuh' && this.password === 'jujulinda') ||
                (this.username === 'Julia' && this.password === '1234567')){
                    alert("Login Realizado com Sucesso!");
                    this.error = null;
                } // Fechamento do if
                else{
                    this.error = "Nome ou senha incorretos";
                } //Fechamento do else 
            }, 1000);
        }, //Fechamento Login

        loginArray(){
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }//Fechamento if

            this.mostrarEntrada = false;
            setTimeout(() => {
                this.mostrarEntrada = true;

                //uso de uma constante para armazenar o número do index onde o usuário "procurando" está "guardaddo" no array
                const index = this.usuarios.indexOf(this.username);
                if(index !== -1 && this.senhas[index] === this.password){
                    this.error = null;
                    this.sucesso = "Login efetuado com sucesso!";

                    localStorage.setItem("username", this.username);
                    localStorage.setItem("password", this.password);

                }//Fechamento if
                else{
                    this.sucesso = null;
                    this.error = "Usuário ou senha incorretos!";
                }//Fechamento do else

                this.username = "";
                this.password = "";

            },500);
                
        }, //Fechamento LoginArray

        adicionarUsuario(){
            this.mostrarEntrada = false;

            //update no conteúdo dos arrays para recuperar os dados armazenados no local storage
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }//Fechamento if
            setTimeout(() => {
                this.mostrarEntrada = true;
                if(!this.usuarios.includes(this.newUsername) && this.newUsername !== ""){
                    if(this.newPassword && this.newPassword === this.confirmPassword){
                        this.usuarios.push(this.newUsername);
                        this.senhas.push(this.newPassword);
                        
                        //Atualizando os valores doas arrays no armazenamento local
                        localStorage.setItem("usuarios", JSON.stringify(this.usuarios)); 
                        localStorage.setItem("senhas", JSON.stringify(this.senhas));

                        this.newUsername = "";
                        this.newPassword = "";
                        this.confirmPassword = "";
                        this.sucesso = "Usuário cadastrado com sucesso!";
                        this.error = null;
                    } //Fechmento if
                    else{
                        this.error = "Por favor, digite uma senha válida";
                        this.sucesso = null;
                    }
                } //Fechamento if
                else{
                    this.error = "Por favor, informe um usuário válido";
                    this.sucesso = null;
                } //Fechamento else
            }, 500);
        }, //Fechamento adicionarUsuario

        paginaCadastro(){
            this.mostrarEntrada =  false;
            setTimeout(() => {
                this.mostrarEntrada = true;
                this.sucesso = "Carregando página de cadastro!";
                this.error = null;

            }, 500);

            setTimeout(() => {
                window.location.href = "cadastro.html";
            }, 1000);
        },

        verCadastrados(){
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }//Fechamento if
            
            this.mostrarLista = !this.mostrarLista; //inverte o valor atual da variável ( ! ) verdadeiro passa pra falso e visse versa
        }, //Fechamento verCadastrados

    }, //Fechamento methods
}).mount("#app"); // Fechamento createApp
