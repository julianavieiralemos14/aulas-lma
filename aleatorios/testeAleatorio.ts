namespace testeAleatorio
{
    let numAleatorio:number;
    let numAleatorioInt:number;

    for(let i = 0; i < 10; i ++)//for: comando de repetição
    {
        numAleatorio = Math.random()*6;// Math.random: retorna número aleatório de 0 a 6
        numAleatorioInt = Math.floor(numAleatorio) + 1;

        numAleatorioInt = Math.floor(Math.random() *6) + 1;

        console.log(`O número inteiro é ${numAleatorioInt}`);
        //console.log(`O número aleatório é ${numAleatorio}`);

        
    }//fim do for

}//fim do namespace

// *6 numero maximo do sorteio aleatório

// 4 + 1 = 5
// 5,99 - 5 + 1 = 6
// 0,98 - 0 + 1 = 1

// 0 + 1 = 1
// 5,99 - 5 + 1 = 6

// numero sorteado (3,5)
// numAleatorio = Math.random() -> 0 a 1 (*6)        
// numAleatorioInt = Math.floor(numAleatorio) = 3,5 -> 3
// numFinal = numAleatorioInt + 1;

// for pucha para baixo então -> 5,9 = 5
// numAleatorio -> 5 + 1 = 6

// esemplo:
// 4,6 
// 4 + 1 = 5
// numFinal = 5 