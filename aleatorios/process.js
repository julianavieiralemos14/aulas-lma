const {createApp} = Vue;
createApp
({
    data()
    {
        return {
            randomIndex: 0,
            randomIndexInternet: 0,
            
            //vetor de imagens locais
            imagensLocais:
            [
                './imagens/lua.jpg',
                './imagens/SENAI_logo.png',
                './imagens/sol.jpg'

                
            ],

            imagensInternet:
            [
                'https://www.clubeparacachorros.com.br/wp-content/uploads/2018/06/labrador-comportamento.jpg',
                'https://leiturinha.com.br/blog/wp-content/uploads/2022/06/iStock-1314796448-1.jpg',
                'https://luanasantanape.files.wordpress.com/2015/05/image11.jpg'
            ],

        }; //fim return
    }, //fim data

    computed:
    {
        randomImage()
        {
            return this.imagensLocais[this.randomIndex];
        },// fim randomImage

        randomImageInternet()
        {
            return this.imagensInternet 
            [this.randomIndexInternet];
        }//fim randomInternet
    },//fim computed

    methods:
    {
        getRandomImage()
        {
            this.randomIndex = Math.floor(Math.random()*this.imagensLocais.length); // length -> tamanho do vetor
            
            this.randomIndexInternet = Math.floor(Math.random() *this.imagensInternet.length);

        }
    },//fim methods

}).mount("#app");